#!/bin/bash

$(while true; do sleep $[ ( $RANDOM % 30 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/tolling-bell_daniel-simion.mp3; done) > /dev/null 2>&1 &
$(while true; do sleep $[ ( $RANDOM % 30 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/elaborate_thunder-Mike_Koenig-1877244752.mp3; done) > /dev/null 2>&1 &
$(while true; do sleep $[ ( $RANDOM % 30 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/Raven\ Sound\ Clip-SoundBible.com-871913074.mp3; done) > /dev/null 2>&1 &
$(while true; do sleep $[ ( $RANDOM % 30 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/wolf5.mp3; done) > /dev/null 2>&1 &
$(while true; do sleep $[ ( $RANDOM % 45 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/haunting.mp3; done) > /dev/null 2>&1 &
$(while true; do echo playing; sleep $[ ( $RANDOM % 120 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/wlaugh.mp3 ; done) > /dev/null 2>&1 &
$(while true; do sleep 1; mpg123 -q --no-control --smooth ./media/elaborate_thunder-Mike_Koenig-1877244752.mp3; done) > /dev/null 2>&1 &
$(while true; do sleep $[ ( $RANDOM % 600 )  + 1 ]s; mpg123 -q --no-control --smooth ./media/The-Addams-Family-Theme-Song.mp3; done) > /dev/null 2>&1 &

echo
echo Staring sound effects... Happy Halloween!
read  -n 1 -p "Press any key to stop" keypressed

echo
echo Exiting...
ps aux | grep $(basename "$0") | grep -v grep | grep -v vi | grep -v $$ | awk '{print $2}' |  xargs kill -KILL
ps axu | grep mpg123 | grep -v grep | awk '{print $2}' | xargs kill -KILL 
echo Halloween sound effects stopped.
